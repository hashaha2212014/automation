**Git global setup**

git config --global user.name "Your name

git config --global user.email "Your email"


**Push an existing folder**

cd existing_folder

git init

git remote add origin https://gitlab.com/hashaha2212014/automation.git

git add .

git commit -m "Initial commit"

git push -u origin "Your Branch"
